# Building Packages
Building artifacts for public consumption is handled on our dev instances via
[`omnibus-gitlab`](https://dev.gitlab.org/gitlab/omnibus-gitlab).  All builds
are initiated via some sort of trigger:
* Nightly pipelines are kicked off [via a Cron Job][nightlies]
* Builds of tagged packages are automatically started when that tag is pushed
  into the repo

[nightlies]: https://dev.gitlab.org/gitlab/omnibus-gitlab/-/pipeline_schedules

# Nightly Builds

## Pipeline
```mermaid
graph LR
  A(Fetch Assets) --> B(Build Ubuntu 16.04 Package)
  B --> C(Upload Ubuntu 16.04 Package)
  C --> D(Build CentOS 6)
  C --> E(Build CentOS 7)
  C --> F(Build Debian 8)
  C --> G(Build Debian 9)
  C --> H(Build OpenSUSE)
  C --> I(Build Scientific Linux)
  C --> J(Build Ubuntu 14.04)
  C --> K(Build Ubuntu 18.04)
```

# Tagged Builds

## Pipeline
```mermaid
graph LR
  A(Fetch Assets) --> B(Build Ubuntu 16.04 Package)
  B --> C(Upload Ubuntu 16.04 Package)
  C --> D(Build CentOS 6)
  C --> E(Build CentOS 7)
  C --> F(Build Debian 8)
  C --> G(Build Debian 9)
  C --> H(Build OpenSUSE)
  C --> I(Build Scientific Linux)
  C --> J(Build Ubuntu 14.04)
  C --> K(Build Ubuntu 18.04)

  D --> L(Dependency Scanner)
  E --> L
  F --> L
  G --> L
  H --> L
  I --> L
  J --> L
  K --> L

  L --> M(Upload CentOS 6 Package)
  L --> N(Upload CentOS 7 Package)
  L --> O(Upload Debian 8 Package)
  L --> P(Upload Debian 9 Package)
  L --> Q(Upload OpenSUSE Package)
  L --> R(Upload Scientific Linux Package)
  L --> S(Upload Ubuntu 14.04 Package)
  L --> T(Upload Ubuntu 18.04 Package)

  M --> U>Release CentOS 6 Package]
  N --> V>Release CentOS 7 Package]
  O --> W>Release Debian 8 Package]
  P --> X>Release Debian 9 Package]
  Q --> Y>Release OpenSUSE Package]
  R --> Z>Release Scientific Linux Package]
  S --> AA>Release Ubuntu 14.04 Package]
  S --> BB>Release Ubuntu 16.04 Package]
  T --> CC>Release Ubuntu 18.04 Package]
  S --> DD>Release Docker Images]
  S --> EE>Release AMI Images]

  EE --> FF(Build and Upload Raspberry Pi Package)

  FF --> GG>Release Raspberry Pi Package]
```

Legend:

```mermaid
graph LR
A(Automatically run job)
B>Manually triggered job]
A -.- B
```

* The Tagged pipeline will start whenever a new tag is pushed to
  `omnibus-gitlab`, however, some steps are manually kicked off as to prevent
  unnecessary artifacts.  When a release manager runs the `release` process,
  these manually triggered jobs will be started.
