# Patch release pressure

**Note: The information here will be apply once the [mainteinance
policy has been extended] to account for three releases**

The patch release pressure is defined as the number of unreleased
merge requests merged into the active stable branches according to
the [maintenance policy].

## Release manager dashboard

Patch release pressure information can be found in the [release manager dashboard].
It is split in two areas:

* **Summary**: Details the patch release pressure for the three active versions.
* **Pressure per version**: Details the patch release pressure per active version.

### Summary

The Summary section includes:

* **S1/S2 pressure**: The number of S1/S2 merge requests merged into stable branches
  that haven't been released.
* **Total pressure**: The number of merge requests merged into stable branches that 
  haven't been released regardless of severity.

### Patch release pressure

The Pressure per Version section includes:

* **S1/S2 MR pressure**: The number of S1/S2 merge requests merged into stable branches
  per version.
* **Total MR pressure**: The number of merge requests merged into stable branches per
  version regardless of severity.

| Summary | Pressure per version |
| ---- | ---- |
| ![Summary](images/summary_panels.png) | ![breakdown per version](images/patch_release_pressure_panels.png) |


## When to perform a patch release?

Release Managers can use the information from the patch release pressure
to determine if a patch release may be required by considering the information
from the Summary section:

1. If the S1/S2 pressure is greater than two, this means there are priority merge
   requests waiting to be patched, in this case, a patch release is **strongly encouraged**.
1. If the Total pressure displays a considerable number of unreleased merge requests (e.g. 10),
   a patch release would be beneficial to reduce the pressure, this is recommended but not
   required. [Other factors](process.md#when-to-perform-a-patch-release) may help
   direct this decision.

## Determine if there are unreleased merge requests to be patched

Per the maintenance policy, patch releases can be prepared for the three active versions, however,
it is possible for only one or two versions to have have unreleased merge requests waiting to be
patched. To determine which versions have unreleased merge requests, release managers can use the
information from the "Total MR pressure" graph:

1. Review the pressure for each version.
1. If the pressure is greater than zero that means that version has merge requests waiting to
   be patched,
1. If the pressure is equal to zero, there are no merge requests waiting to be patched
   for that version.

Example:

![breakdown per version](images/patch_release_pressure_panels.png)

From the above image, release managers can infer:

1. There are unreleased merge requests waiting to be patched for 15.5
1. 15.6 and 15.7 doesn't have unreleased merge requests.
1. Therefore the next patch release will only be for 15.5.

[mainteinance policy has been extended]: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/828
[maintenance policy]: https://docs.gitlab.com/ee/policy/maintenance.html
[release manager dashboard]: https://dashboards.gitlab.net/d/delivery-release_management/delivery-release-management?orgId=1&refresh=5m
