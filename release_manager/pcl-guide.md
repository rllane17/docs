# Production Change Lock (PCL) guide for release managers

See the [handbook page on PCLs](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/#production-change-lock-pcl)
for an overview of the two types of PCLs.

## Hard PCLs

Deployments to production are blocked during a hard PCL. If a hard PCL is taking place:

1. Lock the production environments:

   ```sh
   /chatops run deploy lock gprd-cny
   /chatops run deploy lock gprd # locking gprd-cny will prevent gprd promotions, but the additional 
   # lock adds extra safety in case another release manager misses the PCL notification and tries to
   # promote to production
   ```

1. In the case the PCL impacts staging, pause auto-deploy with `/chatops run auto_deploy pause` to prevent deployments to `gstg-cny`

1. Consider opening a production change request (CR) if one does not already exist to describe any intent to deploy during the PCL. If the PCL is considerably long, for example, we want to decrease risk by deploying in a planned cadence, perhaps once daily, to decrease the deploy pressure ([example CR](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/8677)). The change request dates and process should be discussed with a Delivery Engineering Manager and the Director of Platform.

### Deploying a revert MR during a hard PCL

If a CR exists for the PCL, check it first to see if there are alternative or additional steps for deploying to production.

If a specific change needs to be deployed during a hard PCL, for example in the case that a 
commit needs to be reverted due to an incident:

1. Merge the MR and set labels `Pick into auto-deploy` and `severity::2`.
1. Manually trigger the `auto_deploy:pick` [scheduled pipeline](https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules).
1. Manually trigger the [auto_deploy:tag scheduled pipeline](https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules).

Optionally, to speed up the process, enable the `auto_deploy_tag_latest`. See the [deploying risky MRs in isolation](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/deploy/deploying-risky-mrs-in-isolation.md)
guide for more details.

Alternatively, creating a new auto-deploy branch by manually running the [`auto_deploy:prepare` scheduled pipeline](https://ops.gitlab.net/gitlab-org/release/tools/-/pipeline_schedules) is also an option, but this will likely include other commits that have not been deployed.

Always work closely with the EOC when coordinating these deployments and broadcast your intent widely to inform anyone else involved in the PCL decision making process.

## Soft PCLs

During a soft PCL, no environments need to be locked, but production deployments require EOC approval.
Release managers should work with the EOC to coordinate production deploys during this period.
